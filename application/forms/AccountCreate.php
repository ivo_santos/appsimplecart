<?php

class Application_Form_AccountCreate extends Zend_Form
{

    public function init()
    {

        $this->setMethod('post');

        $this->addElement('text', 'fullname', array(
            'label' => 'Nome completo',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));

        $this->addElement('text', 'document', array(
            'label' => 'CPF',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));

        $this->addElement('text', 'birthdate', array(
            'label' => 'Nascimento',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));

        $this->addElement('text', 'address', array(
            'label' => 'Logradouro',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));


        $this->addElement('text', 'number', array(
            'label' => 'Número',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));


        $this->addElement('text', 'complement', array(
            'label' => 'Complemento',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));


        $this->addElement('text', 'district', array(
            'label' => 'Bairro',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));


        $this->addElement('text', 'city', array(
            'label' => 'Cidade',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));

        $this->addElement('text', 'city', array(
            'label' => 'Cidade',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));

        $this->addElement('select', 'state_id', array(
            'label' => 'Estado',
            'required' => true,
        ));

        $this->addElement('text', 'email', array(
            'label' => 'Email',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
            )
        ));

        $this->addElement('password', 'password', array(
            'label' => 'Senha',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));

        $this->addElement('submit', 'submit', array(
            'ignore' => true,
            'label' => 'Cadastrar',
            'class' => 'btn btn-primary',
        ));


    }


}

