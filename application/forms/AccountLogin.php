<?php

class Application_Form_AccountLogin extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');

        $this->addElement('text', 'email', array(
            'label' => 'Email',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
            )
        ));

        $this->email->addErrorMessage('Preencha seu email');

        $this->addElement('password', 'password', array(
            'label' => 'Senha',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array()
        ));

        $this->password->addErrorMessage('Preencha sua senha');

        $this->addElement('submit', 'submit', array(
            'ignore' => true,
            'label' => 'Acessar',
            'class' => 'btn btn-primary'
        ));

    }


}

