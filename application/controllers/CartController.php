<?php

class CartController extends Zend_Controller_Action
{

    /**
     * product in cart
     */
    public function indexAction()
    {
        $productTable = new Application_Model_ProductMapper();
        $categoryTable = new Application_Model_CategoryMapper();
        $categories = $categoryTable->fetchAll();

        $session = new Zend_Session_Namespace('cart');

        $products = [];
        $total = 0;
        $cart_message = "";

        if(!empty($session->cart_message)){
            $cart_message = $session->cart_message;
            unset($session->cart_message);
        }

        if (!empty($session->products)) {
            foreach ($session->products as $key => $product) {
                $products[$key] = $productTable->find($product['product_id']);
                $products[$key]->cart_quantity = $product['quantity'];
                $products[$key]->cart_total = ($products[$key]->getPrice() * $products[$key]->cart_quantity);
                $total += $products[$key]->cart_total;
            }
        }

        $this->view->cart_message = $cart_message;
        $this->view->categories = $categories;
        $this->view->products = $products;
        $this->view->total = $total;
    }

    /**
     * add product to cart
     */
    public function addToCartAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $session = new Zend_Session_Namespace('cart');

            if (!isset($session->products)) {
                $products[] = [
                    'product_id' => $request->getPost()['product_id'],
                    'quantity' => 1
                ];
                $session->products = $products;
                $session->cart_message = 'Produto adicionado ao seu carrinho!';
                $response = ['success' => true, 'message' => 'Produto adicionado ao seu carrinho!'];
            } else if (isset($session->products)) {

                $in_cart = false;

                if (!empty($session->products)) {

                    foreach ($session->products as $key => $product) {
                        if ($product['product_id'] == $request->getPost()['product_id']) {
                            $in_cart = true;
                            $session->products[$key]['quantity'] = $session->products[$key]['quantity'] + 1;
                        }
                    }

                    if (!$in_cart) {
                        $session->products[] = [
                            'product_id' => $request->getPost()['product_id'],
                            'quantity' => 1
                        ];
                    }
                } else {
                    $products = [
                        'product_id' => $request->getPost()['product_id'],
                        'quantity' => 1
                    ];
                    $session->products = $products;
                }

                $session->cart_message = 'Produto adicionado ao seu carrinho!';
                $response = ['success' => true, 'message' => 'Produto adicionado ao seu carrinho!'];
            }

        } else {
            $response = ['success' => false, 'message' => 'Desculpe, Ocorreu algum erro!'];
        }

        echo json_encode($response);
        die();
    }

    /**
     * remove product from cart
     */
    public function removeToCartAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $session = new Zend_Session_Namespace('cart');

            if (!empty($session->products)) {
                foreach ($session->products as $key => $product) {
                    if ($product['product_id'] == $request->getPost()['product_id']) {
                        unset($session->products[$key]);
                    }
                }
                $session->cart_message = 'Produto removido do seu carrinho!';
                $response = ['success' => true];
            } else {
                $response = ['success' => false, 'message' => 'Desculpe, ocorreu algum erro!'];
            }

            echo json_encode($response);
            die();
        }

    }


    /**
     * update quantity from cart
     */
    public function updateCartAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $session = new Zend_Session_Namespace('cart');

            if (!empty($session->products)) {
                foreach ($session->products as $key => $product) {
                    if ($product['product_id'] == $request->getPost()['product_id']) {
                        if(!empty($request->getPost()['product_id'])){
                            $session->products[$key]['quantity'] = (!empty($request->getPost()['quantity']) && $request->getPost()['quantity'] != 0)? $request->getPost()['quantity']: 1;
                        }
                    }
                }

                $session->cart_message = 'Quantidade atualizada no seu carrinho!';
                $response = ['success' => true];
            } else {
                $response = ['success' => false, 'message' => 'Descupe, ocorreu algum erro!'];
            }

            echo json_encode($response);
            die();
        }

    }
}

