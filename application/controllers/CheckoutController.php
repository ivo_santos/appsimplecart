<?php

class CheckoutController extends Zend_Controller_Action
{

    /**
     * @var Application_Model_CategoryMapper
     */
    private $categoryTable;
    /**
     * @var Application_Model_CustomerMapper
     */
    private $customerTable;
    /**
     * @var Application_Model_StateMapper
     */
    private $stateTable;
    /**
     * @var Application_Model_ProductMapper
     */
    private $productTable;

    public function init()
    {
        $this->categoryTable = new Application_Model_CategoryMapper();
        $this->customerTable = new Application_Model_CustomerMapper();
        $this->stateTable = new Application_Model_StateMapper();
        $this->productTable = new Application_Model_ProductMapper();
    }

    /**
     * info about sale
     */
    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'login');
        }

        $customer = $this->customerTable->find($auth->getIdentity()->id);
        $customer->state = $this->stateTable->find($customer->getState_id());

        $session = new Zend_Session_Namespace('cart');

        $products = [];
        $total = 0;

        if (!empty($session->products)) {
            foreach ($session->products as $key => $product) {
                $products[$key] = $this->productTable->find($product['product_id']);
                $products[$key]->cart_quantity = $product['quantity'];
                $products[$key]->cart_total = ($products[$key]->getPrice() * $products[$key]->cart_quantity);
                $total += $products[$key]->cart_total;
            }
        } else {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'home');
        }

        $this->view->customer = $customer;
        $this->view->categories = $this->categoryTable->fetchAll();
        $this->view->products = $products;
        $this->view->total = $total;
    }

    public function finishAction()
    {
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'login');
        }

        $customer = $this->customerTable->find($auth->getIdentity()->id);
        $session = new Zend_Session_Namespace('cart');

        $total = 0;
        if (!empty($session->products)) {
            foreach ($session->products as $key => $product) {
                $products[$key] = $this->productTable->find($product['product_id']);
                $products[$key]->cart_quantity = $product['quantity'];
                $products[$key]->cart_total = ($products[$key]->getPrice() * $products[$key]->cart_quantity);
                $total += $products[$key]->cart_total;
            }

            /**
             * add order
             * @var Application_Model_OrderMapper
             */
            $orderTable = new Application_Model_OrderMapper();
            $order_id = $orderTable->save(
                (new Application_Model_Order())
                    ->setCustomer_id($customer->getId())
                    ->setTotal($total)
                    ->setShipping_address($customer->getAddress())
                    ->setShipping_number($customer->getNumber())
                    ->setShipping_complement($customer->getComplement())
                    ->setShipping_district($customer->getDistrict())
                    ->setShipping_city($customer->getCity())
                    ->setShipping_state_id($customer->getState_id())
            );

            /**
             * add products to order
             */
            foreach ($session->products as $key => $product) {
                /**
                 * @var Application_Model_OrderProductMapper
                 */
                $orderProductTable = new Application_Model_OrderProductMapper();
                $orderProductTable->save(
                    (new Application_Model_OrderProduct())
                        ->setOrder_id($order_id)
                        ->setProduct_id($product['product_id'])
                        ->setQuantity($product['quantity'])
                );
            }

            /**
             * kill session products
             */
            unset($session->products);

            $this->_helper->getHelper('Redirector')->gotoRoute([], 'my-account-orders');

        } else {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'home');
        }

    }

}

