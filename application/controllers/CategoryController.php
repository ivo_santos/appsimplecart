<?php

class CategoryController extends Zend_Controller_Action {

    /**
     * products from category
     */
    public function indexAction() {
        $url_friendly = $this->getParam('url_friendly');
        $categoryTable = new Application_Model_CategoryMapper();
        $this->view->category  = $categoryTable->findByUrlFriendly($url_friendly);

        $this->view->categories = $categoryTable->fetchAll();

        $productTable = new Application_Model_ProductMapper();
        $this->view->products = $productTable->fetchAllByCategoryId($this->view->category->getId());
    }

}
