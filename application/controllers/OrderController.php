<?php

class OrderController extends Zend_Controller_Action
{
    /**
     * @var Application_Model_CustomerMapper
     */
    private $customerTable;
    /**
     * @var Application_Model_StateMapper
     */
    private $stateTable;

    public function init()
    {
        $this->customerTable = new Application_Model_CustomerMapper();
        $this->stateTable = new Application_Model_StateMapper();
    }

    public function indexAction()
    {
        /**
         * Vefify if it's logged or not
         */
        $auth = Zend_Auth::getInstance();
        if (!$auth->hasIdentity()) {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'login');
        }

        $orderTable = new Application_Model_OrderMapper();
        $this->view->orders = $orderTable->fetchAllByCustomerId($auth->getIdentity()->id);

    }


}