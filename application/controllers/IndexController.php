<?php

class IndexController extends Zend_Controller_Action
{

    /**
     * Home page
     */
    public function indexAction()
    {
        $categoryTable = new Application_Model_CategoryMapper();
        $this->view->categories = $categoryTable->fetchAll();

        $productTable = new Application_Model_ProductMapper();
        $this->view->products = $productTable->fetchAllFeatured();
    }

}

