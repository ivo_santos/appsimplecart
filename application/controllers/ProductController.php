<?php

class ProductController extends Zend_Controller_Action {

    /**
     * @var Application_Model_CategoryMapper
     */
    private $categoryTable;
    /**
     * @var Application_Model_ProductMapper
     */
    private $productTable;
    /**
     * @var Application_Model_AttributeMapper
     */
    private $attributeTable;

    /**
     * init function
     */
    public function init(){
        $this->categoryTable = new Application_Model_CategoryMapper();
        $this->productTable = new Application_Model_ProductMapper();
        $this->attributeTable = new Application_Model_AttributeMapper();
    }

    /**
     * details funcion
     */
    public function indexAction() {
        $this->view->categories = $this->categoryTable->fetchAll();
        $this->view->product = $this->productTable->findByUrlFriendly($this->getParam('url_friendly'));
        $this->view->product_attributes = $this->attributeTable->feachAllByProductId($this->view->product->getId());
    }

    /**
     * search function
     */
    public function searchAction() {
        $this->view->search = $this->getParam('search');
        $this->view->categories = $this->categoryTable->fetchAll();
        $this->view->products = $this->productTable->fetchAllBySearch($this->getParam('search'));
    }

}
