<?php

class AccountController extends Zend_Controller_Action
{
    /**
     * @var Application_Model_CategoryMapper
     */
    private $categoryTable;
    /**
     * @var Application_Model_CustomerMapper
     */
    private $customerTable;
    /**
     * @var Application_Model_StateMapper
     */
    private $stateTable;

    /**
     * init function
     */
    public function init()
    {
        $this->categoryTable = new Application_Model_CategoryMapper();
        $this->customerTable = new Application_Model_CustomerMapper();
        $this->stateTable = new Application_Model_StateMapper();
    }

    /**
     * dashboard function
     */
    public function indexAction()
    {
        /**
         * Vefify if it's logged or not
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'login');
        }

    }

    /**
     * create account function
     * @throws Zend_Form_Exception
     */
    public function createAction()
    {
        /**
         * Vefify if it's logged or not
         */
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'my-account');
        }

        $request = $this->getRequest();

        $form = new Application_Form_AccountCreate();
        $form->getElement('state_id')->addMultiOptions($this->stateTable->fetchAll());

        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {

                $data = $form->getValues();

                $password_key = substr(md5(uniqid(rand(), true)), 0, 10);
                $password = md5($data['password'] . $password_key);

                $dates = explode("/", $data['birthdate']);
                $birthdate = $dates[2] . '-' . $dates[1] . '-' . $dates[0];

                $this->customerTable->save(
                    (new Application_Model_Customer())
                        ->setFullname($data['fullname'])
                        ->setDocument($data['document'])
                        ->setBirthdate($birthdate)
                        ->setEmail($data['email'])
                        ->setPassword($password)
                        ->setPassword_key($password_key)
                        ->setAddress($data['address'])
                        ->setNumber($data['number'])
                        ->setComplement($data['complement'])
                        ->setDistrict($data['district'])
                        ->setCity($data['city'])
                        ->setState_id($data['state_id'])
                );

                $this->auth($data['email'], $data['password']);
            }
        }

        $this->view->categories = $this->categoryTable->fetchAll();
        $this->view->form = $form;
    }

    /**
     * Function to login customer
     * @throws Zend_Form_Exception
     */
    public function loginAction()
    {

        /**
         * Vefify if it's logged or not
         */
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'my-account');
        }

        $request = $this->getRequest();
        $form = new Application_Form_AccountLogin();

        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $this->auth($form->getValue('email'), $form->getValue('password'));
            }
        }

        $this->view->form = $form;
        $this->view->categories = $this->categoryTable->fetchAll();
    }

    /**
     * Function to auth customer
     * @param $email
     * @param $password
     */
    private function auth($email, $password){

        $adapter = new Zend_Auth_Adapter_DbTable(
            $this->_getParam('db'),
            'customers',
            'email',
            'password',
            'MD5(CONCAT(?, password_key))'
        );

        $adapter->setIdentity($email);
        $adapter->setCredential($password);
        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        if ($result->isValid()) {
            $customer = $adapter->getResultRowObject();
            $auth->getStorage()->write($customer);
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'my-account');
        }else{
            $this->_helper->getHelper('Redirector')->gotoRoute([], 'login');
        }
    }

    /**
     * Logout
     */
    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector('index');
    }

}