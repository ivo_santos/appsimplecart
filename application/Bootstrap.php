<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initRewrite () {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        $config = new Zend_Config_Ini(realpath('./') . '/../application/configs/routes.ini', 'production');
        $router->addConfig($config, 'routes');
    }

}
