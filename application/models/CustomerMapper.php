<?php

class Application_Model_CustomerMapper extends Application_Model_AbstractMapper
{

    protected $table;

    function __construct()
    {
        $this->setTable('Application_Model_DbTable_Customer');
    }

    public function save(Application_Model_Customer $customer)
    {
        $data = array(
            'fullname' => $customer->getFullname(),
            'document' => $customer->getDocument(),
            'birthdate' => $customer->getBirthdate(),
            'email' => $customer->getEmail(),
            'password' => $customer->getPassword(),
            'password_key' => $customer->getPassword_key(),
            'address' => $customer->getAddress(),
            'number' => $customer->getNumber(),
            'complement' => $customer->getComplement(),
            'district' => $customer->getDistrict(),
            'city' => $customer->getCity(),
            'state_id' => $customer->getState_id(),
            'created_at' => ($customer->getCreated_at()) ? $customer->getCreated_at() : date('Y-m-d H:i:s'),
        );

        if (null === ($id = $customer->getId())) {
            $this->getTable()->insert($data);
        } else {
            $this->getTable()->update($data, array('id = ?' => $id));
        }
    }

    public function find($id)
    {
        $result = $this->getTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $product = new Application_Model_Customer();
        return $product
            ->setId($row->id)
            ->setFullname($row->fullname)
            ->setDocument($row->document)
            ->setBirthdate($row->birthdate)
            ->setEmail($row->email)
            ->setPassword($row->password)
            ->setPassword_key($row->password_key)
            ->setAddress($row->address)
            ->setNumber($row->number)
            ->setComplement($row->complement)
            ->setDistrict($row->district)
            ->setCity($row->city)
            ->setState_id($row->state_id)
            ->setCreated_at($row->created_at);
    }

}
