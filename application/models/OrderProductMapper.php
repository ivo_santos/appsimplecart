<?php

class Application_Model_OrderProductMapper extends Application_Model_AbstractMapper
{

    protected $table;

    function __construct()
    {
        $this->setTable('Application_Model_DbTable_OrderProduct');
    }

    public function save(Application_Model_OrderProduct $order_product)
    {
        $data = array(
            'order_id' => $order_product->getOrder_id(),
            'product_id' => $order_product->getProduct_id(),
            'quantity' => $order_product->getQuantity(),
        );

        return $this->getTable()->insert($data);
    }

    public function find($id)
    {
        $result = $this->getTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $order_product = new Application_Model_OrderProduct();

        return $order_product
            ->setId($row->id)
            ->setOrder_id($row->order_id)
            ->setProduct_id($row->product_id)
            ->setQuantity($row->quantity);
    }

}
