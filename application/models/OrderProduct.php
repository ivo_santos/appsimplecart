<?php

class Application_Model_OrderProduct
{

    protected $id;
    protected $order_id;
    protected $product_id;
    protected $quantity;

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setOrder_id($order_id) {
        $this->order_id = $order_id;
        return $this;
    }

    public function getOrder_id() {
        return $this->order_id;
    }

    public function setProduct_id($product_id) {
        $this->product_id = $product_id;
        return $this;
    }

    public function getProduct_id() {
        return $this->product_id;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
        return $this;
    }

    public function getQuantity() {
        return $this->quantity;
    }


}

