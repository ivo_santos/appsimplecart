<?php

class Application_Model_State
{

    protected $id;
    protected $code;
    protected $state;

    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCode($code)
    {
        $this->code = (string)$code;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setState($state)
    {
        $this->state = (string)$state;
        return $this;
    }

    public function getState()
    {
        return $this->state;
    }
}

