<?php

class Application_Model_Customer {

    protected $id;
    protected $fullname;
    protected $document;
    protected $birthdate;
    protected $email;
    protected $password;
    protected $password_key;
    protected $address;
    protected $number;
    protected $complement;
    protected $district;
    protected $city;
    protected $state_id;
    protected $created_at;

    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setFullname($fullname) {
        $this->fullname = $fullname;
        return $this;
    }

    public function getFullname() {
        return $this->fullname;
    }

    public function setDocument($document) {
        $this->document = $document;
        return $this;
    }

    public function getDocument() {
        return $this->document;
    }

    public function setBirthdate($birthdate) {
        $this->birthdate = $birthdate;
        return $this;
    }

    public function getBirthdate() {
        return $this->birthdate;
    }

    public function setEmail($email) {
        $this->email = (string) $email;
        return $this;
    }

    public function getEmail() {
        return $this->email;
    }


    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword_key($password_key) {
        $this->password_key = $password_key;
        return $this;
    }

    public function getPassword_key() {
        return $this->password_key;
    }

    public function setAddress($address) {
        $this->address = $address;
        return $this;
    }

    public function getAddress() {
        return $this->address;
    }


    public function setNumber($number) {
        $this->number = $number;
        return $this;
    }

    public function getNumber() {
        return $this->number;
    }


    public function setComplement($complement) {
        $this->complement = $complement;
        return $this;
    }

    public function getComplement() {
        return $this->complement;
    }


    public function setDistrict($district) {
        $this->district = $district;
        return $this;
    }

    public function getDistrict() {
        return $this->district;
    }

    public function setCity($city) {
        $this->city = $city;
        return $this;
    }

    public function getCity() {
        return $this->city;
    }

    public function setState_id($state_id) {
        $this->state_id = $state_id;
        return $this;
    }

    public function getState_id() {
        return $this->state_id;
    }

    public function setCreated_at($ts) {
        $this->created_at = $ts;
        return $this;
    }

    public function getCreated_at() {
        return $this->created_at;
    }
}
