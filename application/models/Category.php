<?php

class Application_Model_Category {

    protected $id;
    protected $name;
    protected $url_friendly;
    protected $order;

    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setName($name) {
        $this->name = (string) $name;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setUrl_friendly($url) {
        $this->url_friendly = (string) $url;
        return $this;
    }

    public function getUrl_friendly() {
        return $this->url_friendly;
    }

    public function setOrder($order) {
        $this->order = (int) $order;
        return $this;
    }

    public function getOrder() {
        return $this->order;
    }

}
