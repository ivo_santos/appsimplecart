<?php

class Application_Model_StateMapper extends Application_Model_AbstractMapper
{
    protected $table;

    function __construct()
    {
        $this->setTable('Application_Model_DbTable_State');
    }

    public function fetchAll()
    {
        $results = $this->getTable()->fetchAll()->toArray();

        $data[0] = "Selecione o estado";

        foreach ($results as $result) {
            $data[$result['id']] = $result['state'];
        }

        return $data;
    }

    public function find($id)
    {
        $result = $this->getTable()->find($id);

        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $state = new Application_Model_State();
        return $state
            ->setId($row->id)
            ->setCode($row->code)
            ->setState($row->state);
    }

}

