<?php

class Application_Model_AttributeMapper extends Application_Model_AbstractMapper
{

    protected $table;

    function __construct()
    {
        $this->setTable('Application_Model_DbTable_Attribute');
    }

    public function feachAllByProductId($product_id)
    {
        $results = $this->getTable()->fetchAll($this->getTable()->select()
            ->setIntegrityCheck(false)
            ->from(['a' => 'attributes'])
            ->joinInner(['pa' => 'product_attributes'], 'a.id = pa.attribute_id')
            ->where('pa.product_id = ?', $product_id)
        );

        $data = [];

        foreach ($results as $result) {
            $model = new Application_Model_Attribute();
            $model->setId($result->id)
                ->setName($result->name)
                ->setOrder($result->order)
                ->setContent($result->content);
            $data[] = $model;
        }

        return $data;
    }

}
