<?php

class Application_Model_CategoryMapper extends Application_Model_AbstractMapper{

    protected $table;

    function __construct()
    {
        $this->setTable('Application_Model_DbTable_Category');
    }

    /**
     * Busca uma categoria de acordo com a url amigavel
     * 
     * @param type $url_friendly
     * @return type
     */
    public function findByUrlFriendly($url_friendly) {
        $row = $this->getTable()->fetchRow(
                $this->getTable()->select()
                        ->where('url_friendly = :url_friendly')
                        ->bind([':url_friendly' => $url_friendly])
        );

        if (0 == count($row)) {
            return;
        }

        $category = new Application_Model_Category();
        
        return $category->setId($row->id)
                ->setName($row->name)
                ->setUrl_friendly($row->url_friendly)
                ->setOrder($row->order);
    }

    /**
     * Busca todas categorias
     * @return \Application_Model_Category
     */
    public function fetchAll() {
        $results = $this->getTable()->fetchAll();
        $data = [];

        foreach ($results as $result) {
            $model = new Application_Model_Category();
            $model->setId($result->id)
                    ->setName($result->name)
                    ->setUrl_friendly($result->url_friendly)
                    ->setOrder($result->order);
            $data[] = $model;
        }

        return $data;
    }

}
