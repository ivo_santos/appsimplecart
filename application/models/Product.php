<?php

class Application_Model_Product {

    protected $id;
    protected $name;
    protected $description;
    protected $price;
    protected $image;
    protected $quantity;
    protected $status;
    protected $url_friendly;
    protected $featured;

    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setName($name) {
        $this->name = (string) $name;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setDescription($description) {
        $this->description = (string) $description;
        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setImage($image) {
        $this->image = $image;
        return $this;
    }

    public function getImage() {
        return $this->image;
    }

    public function setQuantity($quantity) {
        $this->quantity = (int) $quantity;
        return $this;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setStatus($status) {
        $this->status = (int) $status;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setUrl_friendly($url) {
        $this->url_friendly = (string) $url;
        return $this;
    }

    public function getUrl_friendly() {
        return $this->url_friendly;
    }

    public function setFeatured($featured) {
        $this->featured = $featured;
        return $this;
    }

    public function getFeatured() {
        return $this->featured;
    }


}
