<?php

class Application_Model_AbstractMapper {


    public function setTable($table)
    {
        if (is_string($table)) {
            $table = new $table();
        }
        if (!$table instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table provided');
        }
        $this->table = $table;
        return $this;
    }

    public function getTable()
    {
        return $this->table;
    }


}