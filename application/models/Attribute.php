<?php

class Application_Model_Attribute{

    protected $id;
    protected $name;
    protected $content;
    protected $order;

    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setName($name) {
        $this->name = (string) $name;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setOrder($order) {
        $this->order = (int) $order;
        return $this;
    }

    public function getOrder() {
        return $this->order;
    }

    public function setContent($content) {
        $this->content =  $content;
        return $this;
    }

    public function getContent() {
        return $this->content;
    }

}
