<?php

class Application_Model_ProductMapper extends Application_Model_AbstractMapper
{

    protected $table;

    function __construct()
    {
        $this->setTable('Application_Model_DbTable_Product');
    }

    public function fetchAllBySearch($search)
    {
        $results = $this->getTable()->fetchAll(
            $this->getTable()->select()->where('name LIKE ?', '%'.$search.'%')
        );

        $data = [];

        foreach ($results as $result) {
            $model = new Application_Model_Product();
            $model->setId($result->id)
                ->setName($result->name)
                ->setDescription($result->description)
                ->setPrice($result->price)
                ->setImage($result->image)
                ->setQuantity($result->quantity)
                ->setUrl_friendly($result->url_friendly)
                ->setStatus($result->status);
            $data[] = $model;
        }

        return $data;
    }

    public function fetchAllByCategoryId($category_id)
    {
        $results = $this->getTable()->fetchAll($this->getTable()->select()
            ->setIntegrityCheck(false)
            ->from(['p' => 'products'])
            ->joinInner(['pc' => 'product_categories'], 'p.id = pc.product_id')
            ->where('pc.category_id = ?', $category_id)
        );

        $data = [];

        foreach ($results as $result) {
            $model = new Application_Model_Product();
            $model->setId($result->id)
                ->setName($result->name)
                ->setDescription($result->description)
                ->setPrice($result->price)
                ->setImage($result->image)
                ->setQuantity($result->quantity)
                ->setUrl_friendly($result->url_friendly)
                ->setStatus($result->status);
            $data[] = $model;
        }

        return $data;
    }

    public function findByUrlFriendly($url_friendly)
    {
        $row = $this->getTable()->fetchRow(
            $this->getTable()->select()->where('url_friendly = ?', $url_friendly)
        );

        if (0 == count($row)) {
            return;
        }

        $product = new Application_Model_Product();
        return $product->setId($row->id)
            ->setName($row->name)
            ->setDescription($row->description)
            ->setPrice($row->price)
            ->setImage($row->image)
            ->setQuantity($row->quantity)
            ->setStatus($row->status)
            ->setUrl_friendly($row->url_friendly);
    }

    public function find($id)
    {
        $result = $this->getTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $product = new Application_Model_Product();
        return $product->setId($row->id)
            ->setName($row->name)
            ->setDescription($row->description)
            ->setPrice($row->price)
            ->setImage($row->image)
            ->setQuantity($row->quantity)
            ->setStatus($row->status)
            ->setUrl_friendly($row->url_friendly);
    }


    public function fetchAllFeatured()
    {
        $results = $this->getTable()->fetchAll(
            $this->getTable()->select()->where('featured = ?', 1)
        );

        $data = [];

        foreach ($results as $row) {
            $product = new Application_Model_Product();
            $product->setId($row->id)
                ->setName($row->name)
                ->setDescription($row->description)
                ->setPrice($row->price)
                ->setImage($row->image)
                ->setQuantity($row->quantity)
                ->setStatus($row->status)
                ->setFeatured($row->featured)
                ->setUrl_friendly($row->url_friendly);
            $data[] = $product;
        }

        return $data;
    }


}
