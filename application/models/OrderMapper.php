<?php

class Application_Model_OrderMapper extends Application_Model_AbstractMapper
{

    protected $table;

    function __construct()
    {
        $this->setTable('Application_Model_DbTable_Order');
    }

    public function save(Application_Model_Order $order)
    {
        $data = array(
            'customer_id' => $order->getCustomer_id(),
            'total' => $order->getTotal(),
            'shipping_address' => $order->getShipping_address(),
            'shipping_number' => $order->getShipping_number(),
            'shipping_complement' => $order->getShipping_complement(),
            'shipping_district' => $order->getShipping_district(),
            'shipping_city' => $order->getShipping_city(),
            'shipping_state_id' => $order->getShipping_state_id(),
            'created_at' => ($order->getCreated_at()) ? $order->getCreated_at() : date('Y-m-d H:i:s'),
            'changed_at' => date('Y-m-d H:i:s')
        );

        return $this->getTable()->insert($data);
    }

    public function find($id)
    {
        $result = $this->getTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $order = new Application_Model_Order();

        return $order
            ->setId($row->id)
            ->setCustomer_id($row->id)
            ->setTotal($row->total)
            ->setShipping_address($row->shipping_address)
            ->setShipping_number($row->shipping_number)
            ->setShipping_complement($row->shipping_complement)
            ->setShipping_district($row->shipping_district)
            ->setShipping_city($row->shipping_city)
            ->setShipping_state_id($row->shipping_state_id)
            ->setCreated_at($row->created_at)
            ->setChanged_at($row->changed_at);
    }

    public function fetchAllByCustomerId($customer_id) {
        $results = $this->getTable()->fetchAll(
            $this->getTable()->select()->where('customer_id = ?', $customer_id)
        );

        $data = [];

        foreach ($results as $row) {
            $model = new Application_Model_Order();
            $model->setId($row->id)
                ->setCustomer_id($row->customer_id)
                ->setTotal($row->total)
                ->setShipping_address($row->shipping_address)
                ->setShipping_number($row->shipping_number)
                ->setShipping_complement($row->shipping_complement)
                ->setShipping_district($row->shipping_district)
                ->setShipping_city($row->shipping_city)
                ->setShipping_state_id($row->shipping_state_id)
                ->setCreated_at($row->created_at)
                ->setChanged_at($row->changed_at);
            $data[] = $model;
        }

        return $data;
    }


}
