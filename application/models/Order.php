<?php

class Application_Model_Order {

    protected $id;
    protected $customer_id;
    protected $total;
    protected $shipping_address;
    protected $shipping_number;
    protected $shipping_complement;
    protected $shipping_district;
    protected $shipping_city;
    protected $shipping_state_id;
    protected $created_at;
    protected $changed_at;

    public function setId($id) {
        $this->id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setTotal($total) {
        $this->total =  $total;
        return $this;
    }

    public function getTotal() {
        return $this->total;
    }

    public function setCustomer_id($customer_id) {
        $this->customer_id = (int) $customer_id;
        return $this;
    }

    public function getCustomer_id() {
        return $this->customer_id;
    }

    public function setShipping_address($shipping_address) {
        $this->shipping_address = $shipping_address;
        return $this;
    }

    public function getShipping_address() {
        return $this->shipping_address;
    }


    public function setShipping_number($shipping_number) {
        $this->shipping_number = $shipping_number;
        return $this;
    }

    public function getShipping_number() {
        return $this->shipping_number;
    }


    public function setShipping_complement($shipping_complement) {
        $this->shipping_complement = $shipping_complement;
        return $this;
    }

    public function getShipping_complement() {
        return $this->shipping_complement;
    }


    public function setShipping_district($shipping_district) {
        $this->shipping_district = $shipping_district;
        return $this;
    }

    public function getShipping_district() {
        return $this->shipping_district;
    }

    public function setShipping_city($shipping_city) {
        $this->shipping_city = $shipping_city;
        return $this;
    }

    public function getShipping_city() {
        return $this->shipping_city;
    }

    public function setShipping_state_id($shipping_state_id) {
        $this->shipping_state_id = $shipping_state_id;
        return $this;
    }

    public function getShipping_state_id() {
        return $this->shipping_state_id;
    }

    public function setCreated_at($ts) {
        $this->created_at = $ts;
        return $this;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function setChanged_at($ts) {
        $this->changed_at = $ts;
        return $this;
    }

    public function getChanged_at() {
        return $this->changed_at;
    }
}
